package main

import (
	"github.com/xanzy/go-gitlab"
	"log"
	"os"
)

func main() {
	token, ok := os.LookupEnv("GITLAB_ACCESS_TOKEN")
	if !ok {
		log.Fatalf("Env variable 'GITLAB_ACCESS_TOKEN' required.")
	}
	group, ok := os.LookupEnv("GITLAB_GROUP")
	if !ok {
		log.Fatalf("Env variable 'GITLAB_GROUP' required.")
	}
	success := true

	git, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	projects, _, err := git.Groups.ListGroupProjects(
		group,
		&gitlab.ListGroupProjectsOptions{
			IncludeSubgroups: gitlab.Bool(true),
			ListOptions:      gitlab.ListOptions{PerPage: 1000},
		})
	if err != nil {
		log.Fatalf("Failed to list projects: %v", err)
	}
	for _, project := range projects {
		projectDetails, _, err := git.Projects.GetProject(project.ID, &gitlab.GetProjectOptions{License: gitlab.Bool(true)})
		if err != nil {
			log.Printf("Failed to list project details for project %s: %v", project.Name, err)
			success = false
		} else {
			if projectDetails.License == nil {
				if projectDetails.Archived {
					log.Printf("project %s has no license, but the project has been archived.", projectDetails.WebURL)
					continue
				} else {
					log.Printf("Project %s has no license.\n", projectDetails.WebURL)
				}

				contributors, _, err := git.Repositories.Contributors(project.ID, &gitlab.ListContributorsOptions{})
				if err != nil {
					log.Fatalf("Failed listing contributors for project %s", projectDetails.WebURL)
				} else {
					log.Printf("Contributors:")
					for _, contributor := range contributors {
						log.Printf(" - %s (%s)", contributor.Name, contributor.Email)
					}
				}
				success = false
			} else {
				log.Printf("Project %s has license %s: %s", projectDetails.WebURL, projectDetails.License.Name, projectDetails.LicenseURL)
			}
		}
	}
	if !success {
		log.Fatal("Some projects are missing licenses!")
	}
}
