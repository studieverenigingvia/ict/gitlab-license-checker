############################
# STEP 1 build executable binary
############################
FROM golang:1.16.2-alpine AS builder
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/mypackage/myapp/
COPY . .
# Fetch dependencies.
# Using go get.
RUN go mod tidy
# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /go/bin/gitlab-license-checker
############################
# STEP 2 build a small image
############################
FROM alpine

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Copy our static executable.
COPY --from=builder /go/bin/gitlab-license-checker /go/bin/gitlab-license-checker
# Run the gitlab-license-checker binary.
ENTRYPOINT ["/go/bin/gitlab-license-checker"]
